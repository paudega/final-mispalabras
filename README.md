# Entrega practica
## Datos
* Nombre: Paula Delgado Garcia
* Titulación: Ingeniería en Telemática
* Depliegue (url):  paudega.pythonanywhere.com
* Video básico (url): https://youtu.be/JjD_Bd3Souc
* Video parte opcional (url): https://youtu.be/JliYRBA6_IY

## Cuenta Admin Site 
* usuario/contraseña: admin/admin

## Cuentas usuarios
* usuario/contraseña

## Resumen parte obligatoria
Hace búsqueda de cualquier palabra y devuelve el significado que aparece en Wikipedia, RAE y la imagen de Wikipedia

## Lista partes opcionales
* Nombre parte: añadido favicon en todas las páginas
* Nombre parte: donde se almacenan las palabras poder seleccionar o deselecionar cualquiera de las búsquedas

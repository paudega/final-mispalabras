from django.urls import path
from . import views

# Nos creamos las urls según nuestro esquema REST de nuestra aplicación cogiendo nuestras peticiones GET
# Enlace a nuestra página principal, logout para salir de nuestra página, la página xml, la página del json y el nombre de la palabra
urlpatterns = [
    path('', views.index, name='index'),
    #path('usuario/<str:llave>',views.pagpalab),
    #path('logout/', views.logout_view, name='logout_view'),
    #path('contenido-xml/', views.conten_xml, name='conten_xml'),
    #path('contenido-json/', views.content_json, name='content_json'),
    path('signup', views.signup, name='signup'),
    path('signin', views.signin, name='singin'),
    path('ayuda', views.ayuda, name='ayuda'),
    path('signout', views.signout, name='signout'),
    path('<name>', views.significado_palabra, name='significado_palabra')

]

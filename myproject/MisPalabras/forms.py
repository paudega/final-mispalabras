from django import forms

# En este fichero añadimos dos formularios
# Formulario que crea la "caja" para escribir la palabra que queremos buscar
class ContentForm(forms.Form):
    content = forms.CharField(label='Content', widget=forms.Textarea)

# Formulario para buscar una palabra en nuestro buscador
class BuscadorForm(forms.Form):
    buscador = forms.CharField(label='Buscador', max_length=100)
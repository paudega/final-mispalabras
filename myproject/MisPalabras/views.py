

from xml.sax import make_parser
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.contrib.auth import logout
from .forms import ContentForm, BuscadorForm
from .models import Pag
from .apps import Wiki, FlickrImagen
import urllib.request, json, xmltojson
from bs4 import BeautifulSoup
from html.parser import HTMLParser
import requests
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout


# Contador para el fichero tests.py (Utilizado en ejercicios hechos durante el curso)
class Counter():
    def __init__(self):
        self.count: int = 0
    
    def increment(self) -> int:
        self.count += 1
        return(self.count)
counter: Counter = Counter()


# Función que coge el nombre de la palabra que hemos añadido en nuestra tabla 
def cambio_palabra(name, selected):
    pag = Pag.objects.get(name=name)
    pag.selected=selected
    pag.save()

# Función que descargar la palabra que busquemos de wikipedia
# Ponemos la URL de wikipedia concatenado con name que es el nombre de la palabra 
# Transforma la url en xml, en la variable p metemos el nombre de la palabra con la definición sacada de wikipedia
# Guardamos nuestra palabra con la definición en la variable p
def desc_word(name):
    url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ name + \
          '&prop=extracts&exintro&explaintext'
    xmlStream = urllib.request.urlopen(url)
    significado = Wiki(xmlStream).significado()
    p = Pag(name=name, content=significado, selected=False)
    p.save()

# Función que busca la palabra de wikipedia
# Ponemos la URL de wikipedia concatenado con name que es el nombre de la palabra 
# Transforma la url en xml, en la variable p metemos el nombre de la palabra con la definición sacada de wikipedia
# Guardamos nuestra palabra con la definición en la variable p
def search_word(name):
    url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ name + \
          '&prop=extracts&exintro&explaintext'
    xmlStream = urllib.request.urlopen(url)
    significado = Wiki(xmlStream).significado()
    p = Pag(name=name, content=significado, selected=False)
    p.save()

# Función que descarga el meme abrimos templates luego la carpeta pages y luego imagenes que es donde hemos guardado las imagenes
# En la página apimeme.com buscamos un meme que queramos
def desc_meme(name):
    # Cambiar practica_final por el nombre que yo le ponga a esa carpeta
    f = open('MisPalabras/templates/pages/imagenes/' + name + '.jpg', 'wb')
    response = requests.get('http://apimeme.com/meme?meme=Obama-No-Listen&top='+ name + '&bottom=%C2%BFo+no%3F')
    # Añadimos nuestro meme a la carpeta de templates
    f.write(response.content)
    f.close()

# Función para descargar la imagen que aparece en wikipedia de nuestra palabra
# Cogemos la url de wikipedia concatenada con la palabra que hayamos elegido
# Para poder descargar la imagen hay que hacerlo en formato json 
def desc_imgWikipedia(name):
    url = 'https://es.wikipedia.org/w/api.php?action=query&titles=' + name + \
          '&prop=pageimages&format=json&pithumbsize=200'
    with urllib.request.urlopen(url) as json_doc:
        # Decodificamos la imagen en json y la descargamos
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        archivo = json.loads(json_str)
    # Imprimimos la imagen, nos creamos una variable img que hace que el archivo entre a query y a pages 
    # Seleccionamos el elemento a recoger y para hacerlo tenemos que entrar en query, pages, en id (que lo tenemos que calcular)
    # thumbnail y source y añadimos height y width para las dimensiones de la imagen
    print(archivo)
    img = archivo['query']['pages']
    id = list(img.keys())
    img = img[str(id[0])]['thumbnail']
    source = img['source']
    height = img['height']
    width = img['width']
    return source,height,width

# Función descarga la imagen de Flickr, cuando descargamos la imagen la tenemos que pasar a xml 
def desc_imgflickr(name):
    url = 'https://www.flickr.com/services/feeds/photos_public.gne?tags=' + name
    xmlStream = urllib.request.urlopen(url)
    enlazar = FlickrImagen(xmlStream).desc_enlace()
    return enlazar

# Función que descarga el contenido de la RAE, cogemos la url de la RAE y la concatenamos con el nombre de la palabra que elijamos
def DescargaRae(name):
    url = 'https://dle.rae.es/' + name
    # Para acceder a la página de la RAE necesitamos el siguiente código proporcinado en el punto 8.8 de la práctica
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlStream = response.read().decode('utf8')
    
    # Con el siguiente módulo extraemos datos e información de la web
    soup = BeautifulSoup(htmlStream, 'html.parser')
    # Imprimimos el título extraido
    print("Title:", soup.title.string)
    # Busca la información que su propiedad es "og:description"
    ogDescription = soup.find('meta', property='og:description')
    # Mostramos la definición de la palabra que esta contenido en og:description
    if ogDescription:
        significado = "Definición RAE:" + ogDescription['content']
    # Busca la imagen que su propiedad es "og:image"
    ogImage = soup.find('meta', property='og:image')
    # Imprimimos la imagen de la palabra que esta contenida en og:image
    if ogImage:
        print("Image (og:image):", ogImage['content'])
    return significado


@csrf_exempt
def login(request):
      if request.method == "POST":
        action = request.POST['action']
        if action == "Login":
            return redirect('login/')
        elif action == "Logout":
            logout(request)
            return redirect('/')

@csrf_exempt
# Función de nuestra página principal
def index(request):
    # Si la petición es POST
    if request.method == 'POST':
        # Si el nombre de nuestra palabra esta incluido en esa petición POST
        if 'name' in request.POST:
            # Si esa palabra esta seleccionada
            if request.POST.get('select'):
                    # Llamamos a la función cambio_palabra que hemos creado y nuestro selected será True
                    cambio_palabra(name=request.POST['name'], selected=True)
            # Si la palabra no esta seleccionada
            elif request.POST.get('deselect'):
                    # Llamamos a la función cambio_palabra que hemos creado y nuestro selected será False
                    cambio_palabra(name=request.POST['name'], selected=False)
        # Si no encontramos la palabra en la petición llamamos al formulario para crear una nueva
        else:
            form = BuscadorForm(request.POST)
            # Si el formulario es valido dentro del POST se imprime dicho formulario (buscador) para poder buscar la nueva palabra
            if form.is_valid():
                print(form.cleaned_data['buscador'])
                # No redirige a la palabra añadida
                return redirect("/" + form.cleaned_data['buscador'])

    
  
      
    # Filtramos la lista de nuestras palabras dependiendo de si estan seleccionadas o no
    # Si esta seleccionadas podemos hacer que pasen a no seleccionadas y viceversa
    selected = Pag.objects.filter(selected=True)
    selectable = Pag.objects.filter(selected=False)
    content_form = BuscadorForm()
    item_list = Pag.objects.all
    # Si el usuario esta registrado y esta dentro de la página puede desconectarse
    if request.user.is_authenticated:
        link = "http://localhost:1234/logout/"
        id = request.user
        accion = "Logout"
        autentificacion = True
    # Si el usuario esta registrado y no esta dentro de la página puede conectarse
    else:
        link = "http://localhost:1234/admin/"
        id = ''
        accion = "Login"
        autentificacion = False
    return(render(request, 'pages/seleccionados.html', {'selecteds': selected,
                                                'selectables': selectable,
                                                'item_list': item_list,
                                                'link': link,
                                                'id': id,
                                                'accion': accion,
                                                'form': content_form,
                                                'autentificacion': autentificacion}))


    
def signup(request):

    if request.method == "POST":
        username = request.POST['username']
        fname = request.POST['fname']
        lname = request.POST['lname']
        email = request.POST['email']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        myuser  = User.objects.create_user(username, email, pass1)
        myuser.first_name = fname
        myuser.last_name = lname

        myuser.save()
        
        messages.success(request, "Your account has been successfully created")
        
        return redirect('index')
    return render(request, "pages/signup.html")

def signin(request):

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['pass1']

        user = authenticate(username=username, password=pass1)

        if user is not None:
            login(request, user)
            fname = user.first_name
            return render(request, "pages/index.html", {'fname': fname})

        else:
            messages.error(request, "Bad Credentials!")
            return redirect('index')
    return render(request, "pages/signin.html")

def signout(request):
    logout(request)
    messages.success(request, 'Logged out Successfully')
    return redirect('index')




@csrf_exempt
def significado_palabra(request, name):
    # Si la petición es POST
    if request.method == 'POST':
        # Guardamos en la variable p el nombre de nuestra palabra
        p = Pag.objects.get(name=name)
        # Mandamos el otro formulario que hemos creado, para poder tener nuestra palabra
        form = ContentForm(request.POST)
        # Si el formulario es valido dentro del POST
        if form.is_valid():
            # Guardamos el contenido en nuestra variable p
            p.content = form.cleaned_data['content']
            p.save()

    # Si la petición es GET o POST
    if request.method == 'GET' or request.method == 'POST':
        # Creamos una excepción 
        try:
            # La palabra guardada en nuestra variable existe y es valida
            p = Pag.objects.get(name=name)
            content_form = ContentForm(initial={'content': p.content})
            status = 200
        except Pag.DoesNotExist:
            # Cuando la palabra no existe usamos la función de descargar una palabra y la guardamos en nuestra variable
            # Una vez que la guardamos ya nos notifican que es valida
            desc_word(name)
            p = Pag.objects.get(name=name)
            content_form = ContentForm(initial={'content': p.content})
            status = 200
        # Lo añadimos al template del content
        content_template = loader.get_template('pages/content.html')
        source, height, width = desc_imgWikipedia(name)
        source2 = desc_imgflickr(name)
        significado = DescargaRae(name)
        desc_meme(name)
        content_html = content_template.render({'page': name,
                                                'form': content_form,
                                                'source': source,
                                                'height': height,
                                                'width': width,
                                                'source2': source2,
                                                'significado': significado,
                                                'name':name},
                                               request)
        return (HttpResponse(content_html, status=status))


@csrf_exempt
# Función para crear el contenido xml de nuestra página
def conten_xml(request):
    # En la variable page guardamos todo el contenido de nuestra página
    pages = Pag.objects.all()
    content_xml = ''
    # Creamos el bucle para un p que este dentro de pages, concantenamos la url con el nombre contenido en las p's
    for p in pages:
        url = 'http://localhost:8000/' + p.name
        # Decodificamos
        with urllib.request.urlopen(url) as response:
            htmlStream = response.read().decode('utf8')
        # Extraemos la información
        soup = BeautifulSoup(htmlStream, 'html.parser')
        file = json.loads(soup)
        print(file)



@csrf_exempt
def content_json(request):
    url = 'http://localhost:8000'
    # Decodificamos
    with urllib.request.urlopen(url) as response:
        htmlStream = response.read().decode('utf8')
    # Extraemos la información
    soup = BeautifulSoup(htmlStream, 'html.parser')
    books = soup.find_all(
        'li')
    res, book_no = [], 1
    # Bucle de book dentro de books
    for book in books:
        # Variable con la concatenación de la url con el a y href de cada book de la lista books
        link = 'http://localhost:8000/' + book.find('a')['href']
        # La variable palabra es donde guardamos el nombre de la palabra para cada book
        palabra = book.find('input', attrs={'name': 'name'})['value']
        # La variable token es donde guardamos el valor del token para cada book
        token = book.find('input', attrs={'name':'csrfmiddlewaretoken'})['value']
        # Información que contiene cada book
        data = {'book no': str(book_no), 'title': palabra, 'link': link,
                'token': token}
        book_no += 1
        res.append(data)
    print(res)

# Función para desconectarse y regresar a la página principal
def logout_view(request):
    logout(request)
    return redirect("/")


def ayuda(request):
    return render(request, 'pages/ayuda.html')

def palabra(request):
    return render(request, 'pages/pagpalab.html')

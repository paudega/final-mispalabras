from django.db import models

# Creamos la tabla de nuestra página, que va a tener un nombre, un contenido por palabra y vamos a tener un selector 
# de palabras que vamos a poder seleccionar la palabra que queramos de nuestra lista
class Pag(models.Model):
    name = models.CharField(max_length=256)
    content = models.TextField()
    selected = models.BooleanField(default=False)






